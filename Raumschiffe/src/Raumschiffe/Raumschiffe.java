
package Raumschiffe;

import java.util.Scanner;

	//Klasse Ladung
		/**
		 * Dies ist die Dokumentation der Klasse Ladung.
		 * In diesem Klasse wurden einige Konstruktoren mit verschieden Attributen f�r die Ladung angelegt.
		 * @author Saleh Al Mohammed Al Doush
		 * @version 1.1
		 * @since 03.02.2021
		 */

		class Ladung {
		
			// Deklaration der Instanzvariablen (Membervariablen, Instanzmerkmale)
			
			/**
			 *Deklaration der Instanzvariablen (Membervariablen, Instanzmerkmale)
			 */
		
				private String bezeichnung;
				private int menge;
		
			//Konstruktur Ladung (1)
				
				 /**
			     * Konstruktor f�r die Klasse Ladung
			     */
		
				Ladung ()
					{
						System.out.println("Im parameterlosen Konstruktor Ladung 1");
					}
			
			//Konstruktur Ladung (2)
				
				 /**
			     * Konstruktor f�r die Klasse Ladung
			     * @paramnamen bezeichnung der Bezeichnung der Ladung
			     */
		
				Ladung (String bezeichnung)
					{
						this.bezeichnung = bezeichnung;
						System.out.println("Die Ladung bezeichnit als : " + bezeichnung);
					}
			
			//Konstruktur Ladung (3)
				
				 /**
			     * Konstruktor f�r die Klasse Ladung
			     * @paramnamen bezeichnung der Bezeichnung der Ladung
			     * @paramnamen menge die Menge der Ladung
			     */
		
				Ladung (String bezeichnung , int menge)
					{
						this.bezeichnung = bezeichnung;
						this.menge = menge;
						System.out.println("Die Ladung bezeichnit als : " + bezeichnung);
						System.out.println("Die Ladung hat Menge von " + bezeichnung + " : " + menge + "\n");
					}
		
					//Getter & Setter bezeichnung (Klasse Ladung)
				
				 /**
			     * Setzen Bezeichnung der Ladung
			     * @param addbezeichnung   Bezeichnung der Ladung
			     */
			
						public void setBezeichnung (String addbezeichnung)
							{
								bezeichnung=addbezeichnung;	
							}

				/**
				 * Liefert die Bezeichnung der Ladung zur�ck.
				 * @return Die Bezeichnung der Ladung
				 */
						
						public String getBezeichnung()
							{
								return bezeichnung;	
							}
					
		
					//Getter & Setter menge
						
						 /**
					     * Setzen Menge der Ladung
					     * @param addmenge   Menge der Ladung
					     */
			
						public void setMenge (int addmenge)
							{
								menge=addmenge;	
							}
						
						/**
						 * Liefert die Menge der Ladung zur�ck.
						 * @return Die Menge der Ladung
						 */
	
						public int getMenge()
							{
								return menge;	
							}
					
					//Getter Ladungsverzeichnis
						
						/**
						 * Liefert die Ladungsverzeichnis der Ladung zur�ck.
						 * @return Die Ladungsverzeichnis der Ladung
						 */				
					
						public String getLadungsverzeichnis ()
							{
							String ladungsverzeichnis = ("Die Ladung bezeichnit als : " + bezeichnung + "\n" + "Die Ladung hat Menge von " + bezeichnung + " : " + menge + "\n");
							System.out.println (ladungsverzeichnis);
							return ladungsverzeichnis;	
							}
				}

	//Klasse Raumschiffe
		
		/**
		 * Dies ist die Dokumentation der Klasse Raumschiffe.
		 * In diesem Klasse wurden einige Konstruktoren mit verschieden Attributen f�r die Raumschiffe angelegt.
		 * Au�erdem wurde einige Methoden angelegt, die die Verh�ltnisse der Raumschiffe steuern.
		 * @author Saleh Al Mohammed Al Doush
		 * @version 1.1
		 * @since 03.02.2021
		 */
		
	class Raumschiffe {
		
		Scanner tastatur = new Scanner(System.in);
		
			//Treffer vermerken
		/**
		 * Diese Methode wird benutzt, um die Treffer zu vermerken.
		 * @param keine Parameter
		 * @return keine R�ckgabewert
		 */
				void zielraumschiff()
					{
						System.out.print("Die Raumschiff " + name + " fragt Sie, was ist das Zielraumschiff: \n");
						String zielraumschiff=tastatur.next();
						System.out.print(zielraumschiff + " wurde getroffen \n");
					}
		
			// Deklaration der Instanzvariablen (Membervariablen, Instanzmerkmale)
				
				/**
				 *Deklaration der Instanzvariablen (Membervariablen, Instanzmerkmale)
				 */
	
				private String name;
				private int energieversorgung;
				private int schutzschilde;
				private int lebenserhaltungssystem;
				private int h�lle;
				private int photonentorpedos;
				private int androiden;
		
		
			//Broadcastkommunikator
				
				/**
				 *Deklaration der Broadcastkommunikator Array, die Nachtrichten enth�lt
				 */
			
				public String [] broadcastkommunikator = {"Phaserkanone abgeschossen" , "-=*Click*=-" , "Photonentorpedo abgeschossen"};
			
			//Nachrichten an Alle
				
			/**
			* Diese Methode wird benutzt, um Eine Nachricht an alle zu senden.
			* @param nachricht Nachtricht an Alle
			* @return keine R�ckgabewert
			*/
				
				public void setNachrichten (String nachricht)
					{
						System.out.println ("\n\nVon Raumschiffe " + name +" Nachrichten an Alle: \n" + nachricht + "\n \n");
					}

			
			//Konstruktur Raumschiffe (1)
				
				 /**
			     * Konstruktor f�r die Klasse Raumschiffe
			     */
		
				public Raumschiffe ()
				{
					System.out.println("Im parameterlosen Konstruktor Raumschiffe 1");
				}
		
			
			//Konstruktur Raumschiffe (2)
				
				 /**
			     * Konstruktor f�r die Klasse Raumschiffe
			     * @paramnamen name der Name der Raumschiffe
			     * @paramnamen energieversorgung die energieversorgung der Raumschiffe
			     * @paramnamen schutzschilde die Schutzschilde der Raumschiffe
			     * @paramnamen lebenserhaltungssystem die Lebenserhaltungssystem der Raumschiffe
			     * @paramnamen h�lle die H�lle der Raumschiffe
			     * @paramnamen photonentorpedos die Photonentorpedos der Raumschiffe
			     * @paramnamen androiden die Androiden der Raumschiffe
			     */
	
				public Raumschiffe (String name,
									int energieversorgung,
									int schutzschilde,
									int lebenserhaltungssystem,
									int h�lle,
									int photonentorpedos,
									int androiden)
					{
									this.name = name;
									this.energieversorgung = energieversorgung;
									this.schutzschilde = schutzschilde;
									this.lebenserhaltungssystem = lebenserhaltungssystem;
									this.h�lle = h�lle;
									this.photonentorpedos = photonentorpedos;
									this.androiden = androiden;
				
									System.out.println("Die Raumschiffe hei�t " + name + "\nDer Zustand des Raumschiffe ist : \n" +
														"Energieversorgung : " + energieversorgung + "\n" +
														"Schutzschilde : " + schutzschilde + "\n" +
														"Lebenserhaltungssystem : " + lebenserhaltungssystem + "\n" +
														"H�lle : " + h�lle + "\n" +
														"Photonentorpedos : " + photonentorpedos + "\n" +
														"Androiden : " + androiden + "\n \n" + 
														"Diese Raumschiffe hat folgende Ladungen" + "\n");
					}
	

			//Konstruktur Raumschiffe (3)
				
				 /**
			     * Konstruktor f�r die Klasse Raumschiffe
			     * @paramnamen name der Name der Raumschiffe
			     * @paramnamen energieversorgung die energieversorgung der Raumschiffe
			     * @paramnamen schutzschilde die Schutzschilde der Raumschiffe
			     * @paramnamen lebenserhaltungssystem die Lebenserhaltungssystem der Raumschiffe
			     * @paramnamen h�lle die H�lle der Raumschiffe
			     * @paramnamen photonentorpedos die Photonentorpedos der Raumschiffe
			     */
			
				public Raumschiffe (String name,
									int energieversorgung,
									int schutzschilde,
									int lebenserhaltungssystem,
									int h�lle,
									int photonentorpedos)
					{
									this.name = name;
									this.energieversorgung = energieversorgung;
									this.schutzschilde = schutzschilde;
									this.lebenserhaltungssystem = lebenserhaltungssystem;
									this.h�lle = h�lle;
									this.photonentorpedos = photonentorpedos;
				
									System.out.println("Die Raumschiffe hei�t " + name + "\nDer Zustand des Raumschiffe ist : \n" +
														"Energieversorgung : " + energieversorgung + "\n" +
														"Schutzschilde : " + schutzschilde + "\n" +
														"Lebenserhaltungssystem : " + lebenserhaltungssystem + "\n" +
														"H�lle : " + h�lle + "\n \n" +
														"Diese Raumschiffe hat folgende Ladungen" + "\n");
					}
			

			//Konstruktur Raumschiffe (4)
				
				 /**
			     * Konstruktor f�r die Klasse Raumschiffe
			     * @paramnamen name der Name der Raumschiffe
			     * @paramnamen energieversorgung die energieversorgung der Raumschiffe
			     * @paramnamen schutzschilde die Schutzschilde der Raumschiffe
			     * @paramnamen lebenserhaltungssystem die Lebenserhaltungssystem der Raumschiffe
			     * @paramnamen h�lle die H�lle der Raumschiffe
			     */
			
				public Raumschiffe (String name,
									int energieversorgung,
									int schutzschilde,
									int lebenserhaltungssystem,
									int h�lle)
					{
									this.name = name;
									this.energieversorgung = energieversorgung;
									this.schutzschilde = schutzschilde;
									this.lebenserhaltungssystem = lebenserhaltungssystem;
									this.h�lle = h�lle;
									System.out.println("Die Raumschiffe hei�t " + name + "\nDer Zustand des Raumschiffe ist : \n" +
														"Energieversorgung : " + energieversorgung + "\n" +
														"Schutzschilde : " + schutzschilde + "\n" +
														"Lebenserhaltungssystem : " + lebenserhaltungssystem + "\n" +
														"H�lle : " + h�lle + "\n \n" + 
														"Diese Raumschiffe hat folgende Ladungen" + "\n");
					}


			//Konstruktur Raumschiffe (5)
				
				 /**
			     * Konstruktor f�r die Klasse Raumschiffe
			     * @paramnamen name der Name der Raumschiffe
			     * @paramnamen energieversorgung die energieversorgung der Raumschiffe
			     * @paramnamen schutzschilde die Schutzschilde der Raumschiffe
			     * @paramnamen lebenserhaltungssystem die Lebenserhaltungssystem der Raumschiffe
			     */
			
				public Raumschiffe (String name,
									int energieversorgung,
									int schutzschilde,
									int lebenserhaltungssystem)
					{
									this.name = name;
									this.energieversorgung = energieversorgung;
									this.schutzschilde = schutzschilde;
									this.lebenserhaltungssystem = lebenserhaltungssystem;				
									System.out.println("Die Raumschiffe hei�t " + name + "\nDer Zustand des Raumschiffe ist : \n" +
														"Energieversorgung : " + energieversorgung + "\n" +
														"Schutzschilde : " + schutzschilde + "\n" +
														"Lebenserhaltungssystem : " + lebenserhaltungssystem + "\n \n" +
														"Diese Raumschiffe hat folgende Ladungen" + "\n");
					}

			//Konstruktur Raumschiffe (6)
				
				 /**
			     * Konstruktor f�r die Klasse Raumschiffe
			     * @paramnamen name der Name der Raumschiffe
			     * @paramnamen energieversorgung die energieversorgung der Raumschiffe
			     * @paramnamen schutzschilde die Schutzschilde der Raumschiffe
			     */
			
				public Raumschiffe (String name,
									int energieversorgung,
									int schutzschilde)
					{
									this.name = name;
									this.energieversorgung = energieversorgung;
									this.schutzschilde = schutzschilde;				
									System.out.println("Die Raumschiffe hei�t " + name + "\nDer Zustand des Raumschiffe ist : \n" +
														"Energieversorgung : " + energieversorgung + "\n" +
														"Schutzschilde : " + schutzschilde + "\n \n" +
														"Diese Raumschiffe hat folgende Ladungen" + "\n");
					}
			
			//Konstruktur Raumschiffe (7)
				
				 /**
			     * Konstruktor f�r die Klasse Raumschiffe
			     * @paramnamen name der Name der Raumschiffe
			     * @paramnamen energieversorgung die energieversorgung der Raumschiffe
			     */
			
				public Raumschiffe (String name,
									int energieversorgung)
					{
									this.name = name;
									this.energieversorgung = energieversorgung;
									System.out.println("Die Raumschiffe hei�t " + name + "\nDer Zustand des Raumschiffe ist : \n" +
														"Energieversorgung : " + energieversorgung + "\n \n" +
														"Diese Raumschiffe hat folgende Ladungen" + "\n");
					}
			
			//Konstruktur Raumschiffe (8)
				
				 /**
			     * Konstruktor f�r die Klasse Raumschiffe
			     * @paramnamen name der Name der Raumschiffe
			     */
			
				public Raumschiffe (String name)
					{
									this.name = name;
									System.out.println("Die Raumschiffe hei�t " + name + "\n \n" +
														"Diese Raumschiffe hat folgende Ladungen" + "\n");
					}
				
			
				//Getter & Setter Name
				
				/**
				* Setzen wert der Name
				* @param r der wert der Name
				*/
	
					public void setName (String r)
						{
							name=r;	
						}
					
				/**
				* Liefert die Wert der Name zur�ck.
				* @return Die Wert der Name einer Raumschiffe
				*/
					
					public String getName()
						{
							return name;	
						}
		
			
				//Getter & Setter Energieversorgung
					
				/**
				* Setzen wert der Energieversorgung
				* @param addenergieversorgung der wert der Energieversorgung
				* Falls die eingegebene Wert gr��er oder gleich 50, wird dieses Wert und broadcastkommunikator Nachtricht gedr�ckt
				* Au�erdem wird diese eingegebene Wert im energieversorgung gespeichert
				* anderesfall wird die Treffer Methode aufgeruft
				* @see Treffer Methode
				*/	
		
					public void setEnergieversorgung (int addenergieversorgung)
						{
						
							//Phaserkanone abschie�en
					
						if(addenergieversorgung <= 50)
							{
								System.out.println ("Die aktulle Energieversorgung ist : " +addenergieversorgung+ " %");
								System.out.println (broadcastkommunikator [1]);
								this.energieversorgung = addenergieversorgung;
							}
						
						else{
							
							// Treffer Methode aufrufen
							
								zielraumschiff();
								System.out.println (broadcastkommunikator [0]);
								this.energieversorgung = addenergieversorgung - 50 ;
								System.out.println ("Die aktulle Energieversorgung ist : " +energieversorgung+ " %");}
					
							}

				/**
				* Liefert die Wert der Energieversorgung zur�ck.
				* @return Die Wert der Energieversorgung einer Raumschiffe
				*/
					
					public int getEnergieversorgung()
						{
							return energieversorgung;	
						}
		
		
				//Getter & Setter Schutzschilde
				
				/**
				* Setzen wert der Schutzschilde
				* @param addschutzschilde der wert der Schutzschilde
				*/	
		
					public void setSchutzschilde (int addschutzschilde)
						{
							schutzschilde=addschutzschilde;	
						}
				
				/**
				* Liefert die Wert der Schutzschilde zur�ck.
				* @return Die Wert der Schutzschilde einer Raumschiffe
				*/
					
					public int getSchutzschilde()
						{
							return schutzschilde;	
						}
		
		
				//Getter & Setter Lebenserhaltungssystem

				/**
				* Setzen wert der Lebenserhaltungssystem
				* @param addlebenserhaltungssystem   der wert der Lebenserhaltungssystem
				*/	
		
					public void setLebenserhaltungssystem (int addlebenserhaltungssystem)
						{
							lebenserhaltungssystem=addlebenserhaltungssystem;	
						}
				
				/**
				* Liefert die Wert der Lebenserhaltungssystem zur�ck.
				* @return Die Wert der Lebenserhaltungssystem einer Raumschiffe
				*/

					public int getLebenserhaltungssystem()
						{
							return lebenserhaltungssystem;	
						}
		
				
				//Getter & Setter H�lle
				
				/**
				* Setzen wert der h�lle
				* @param addh�lle   der wert der h�lle
				*/
					
					public void setH�lle (int addh�lle)
						{
							h�lle=addh�lle;	
						}
					
				/**
				* Liefert die Wert der h�lle zur�ck.
				* @return Die Wert der h�lle einer Raumschiffe
				*/					

					public int getH�lle()
						{
							return h�lle;	
						}

				
				//Getter & Setter Photonentorpedos
					
					 /**
				     * Setzen Anzahl der Photonentorpedos
				     * Falls addphotonentorpedos gr��er oder gleich 0, wird das Wert der Photonentorpedos auf die eingegebene wirt eingesetzt und ein nachricht aufgeruft
				     * @see broadcastkommunikator
				     * anderesfall wird die Treffer Methode aufgeruft
				     * @see Treffer Methode 
				     * @param addphotonentorpedos   die neue Anzahl der Photonentorpedos
				     */
					
					public void setPhotonentorpedos (int addphotonentorpedos)
						{
									//Photonentorpedos abschie�en 
					
								if(addphotonentorpedos <= 0)
									{
										this.photonentorpedos = addphotonentorpedos;
										System.out.println ("Die aktulle Anzahal des Photonentorpedos ist : " +photonentorpedos+" Photonentorpedos \n");;
										System.out.println (broadcastkommunikator [1]);
									}
						
								else if (addphotonentorpedos > 0)
									{
									
									// Treffer Methode aufrufen
									
										zielraumschiff();
										System.out.println (broadcastkommunikator [2]);
										this.photonentorpedos = addphotonentorpedos - 1;
										System.out.println ("Die aktulle Anzahal des Photonentorpedos ist : " +photonentorpedos+" Photonentorpedos \n ");;
						
									}
						}
					
					/**
				     * Liefert die Anzahl der photonentorpedos zur�ck.
				     * @return    Die Anzahl der photonentorpedos einer Raumschiffe
				     */

					public int getPhotonentorpedos()
						{
							return photonentorpedos;	
						}				
		

				//Getter & Setter Androiden
					
					 /**
				     * Setzen Anzahl der androiden
				     * @param addandroiden   der Anzahl der androiden
				     */
				
					public void setAndroiden (int addandroiden)
						{
							androiden=addandroiden;	
						}
					
					/**
				     * Liefert die Anzahl der androiden zur�ck.
				     * @return    Die Anzahl der androiden einer Raumschiffe
				     */
					
					public int getAndroiden()
						{
							return androiden;	
						}
					
					//Getter Zustand
					
					/**
				     * Liefert den Zustand einer Raumschiffe (name,Energieversorgung,schutzschilde,Lebenserhaltungssystem,H�lle,Photonentorpedos,Androiden) zur�ck.
				     * @return    Zustand einer Raumschiffe
				     */
					
					public String getZustand()
						{
							String zustand =  ("Die Raumschiffe hei�t " + name + "\nDer Zustand des Raumschiffe ist : \n" +
												"Energieversorgung : " + energieversorgung + "\n" +
												"Schutzschilde : " + schutzschilde + "\n" +
												"Lebenserhaltungssystem : " + lebenserhaltungssystem + "\n" +
												"H�lle : " + h�lle + "\n" +
												"Photonentorpedos : " + photonentorpedos + "\n" +
												"Androiden : " + androiden + "\n \n" + 
												"Diese Raumschiffe hat folgende Ladungen" + "\n");
						
							System.out.println (zustand);
						
							return zustand;	
						}
				
				
	public static void main(String[] args){
		
		//Anfangszustand
		

			//Raumschiffe Klingonen
		
				Raumschiffe Klingonen = new Raumschiffe ("IKS Hegh'ta" , 100 ,100 ,100 ,100 ,1,2);
				Ladung Klingonen1 = new Ladung ("Ferengi Schneckensaft" , 200);
				Ladung Klingonen2 = new Ladung ("Bat'leth Klingonen Schwert" , 200);

			//Raumschiffe Romulaner
		
				Raumschiffe Romulaner = new Raumschiffe ("IRW Khazara" , 100 ,100 ,100 ,100 ,2,2);
				Ladung Romulaner1 = new Ladung ("Borg-Schrott" , 5);
				Ladung Romulaner2 = new Ladung ("Rote Materie" , 2);
				Ladung Romulaner3 = new Ladung ("Plasma-Waffe" , 50);

			//Raumschiffe Vulkanier
		
				Raumschiffe Vulkanier = new Raumschiffe ("NI'Var" , 80 ,80 ,100 ,50 ,0,5);
				Ladung Vulkanier1 = new Ladung ("Forschungssonde" , 35);
				Ladung Vulkanier2 = new Ladung ("Photonentorpedo" , 3);
		
			
		//Auszuf�hrende Methoden in der Main:	
				
			//Die Klingonen schie�en mit dem Photonentorpedo einmal auf die Romulaner.

				Klingonen.setPhotonentorpedos(1);
		
			//Die Romulaner schie�en mit der Phaserkanone zur�ck.
		
				Romulaner.setEnergieversorgung(100);
		
			//Die Vulkanier senden eine Nachricht an Alle �Gewalt ist nicht logisch�.
		
				Vulkanier.setNachrichten("Gewalt ist nicht logisch");
		
			//Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus
		
				Klingonen.getZustand();
				Klingonen1.getLadungsverzeichnis ();
				Klingonen2.getLadungsverzeichnis ();
		
			//Die Klingonen schie�en mit zwei weiteren Photonentorpedo auf die Romulaner.
		
				Klingonen.setPhotonentorpedos(2);
				Klingonen.setPhotonentorpedos(1);
		
			//Die Klingonen, die Romulaner und die Vulkanier rufen jeweils den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus.
				
				Klingonen.getZustand();
				Klingonen1.getLadungsverzeichnis ();
				Klingonen2.getLadungsverzeichnis ();
				Romulaner.getZustand();
				Romulaner1.getLadungsverzeichnis ();
				Romulaner2.getLadungsverzeichnis ();
				Romulaner3.getLadungsverzeichnis ();
				Vulkanier.getZustand();
				Vulkanier1.getLadungsverzeichnis ();
				Vulkanier2.getLadungsverzeichnis ();

	}


}
